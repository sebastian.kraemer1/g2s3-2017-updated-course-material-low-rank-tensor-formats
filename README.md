# G2S3 (2017) Updated Course Material: Low Rank Tensor Formats

A basic numerical introduction with exercises and solutions to well known tensor formats in Matlab Live Scripts. 

Covered are:
    
    1) Basic functions in Matlab
    2) Canonical Polyiadic (CP) decomposition
    2b) Strassen algorithm
    3) Tucker (HOSVD) format
    4) Tensor Train (MPS) format
    5) Hints at the Hierarchical Tucker (HT) format

Just open Matlab and execute RUN_ME_FIRST.m.
