function T = TT_repr_to_full_tensor(G)

T = G{1};
d = length(G);
n = zeros(1,d);
n(1) = size(G{1},3);

for i = 2:d
    T = boxtimes(T,G{i});
    n(i) = size(G{i},3);
end

T = reshape(T,n);

end