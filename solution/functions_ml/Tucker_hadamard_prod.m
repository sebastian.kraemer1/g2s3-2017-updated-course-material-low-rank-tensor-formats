function [W,P,info] = Tucker_hadamard_product(d,C,U,S,Q)

rC = size(C);
rC = [rC,ones(1,d-length(rC))];

rS = size(S);
rS = [rS,ones(1,d-length(rS))];

P = cell(1,d);

for mu = 1:d
    P{mu} = mode_1_kron(U{mu}',Q{mu}')';
end

CQ = C(:)*S(:)';
W = reshape(CQ,[rC,rS]);
perm = [1:d; d + (1:d)];
W = reshape(  permute(W,perm(:))  ,[rC.*rS]);

info = 0;
end