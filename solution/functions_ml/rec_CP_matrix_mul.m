function C = rec_CP_matrix_mul(A,B,Phi,m,n)

if round(m) ~= m
    C = A*B;
    return;
end

A_block = mat2cell(A,m*ones(1,n),m*ones(1,n));
B_block = mat2cell(B,m*ones(1,n),m*ones(1,n));
C_block = repmat({zeros(m,m)},[n,n]);

R = size(Phi{1},2);

%%%% solution EX3

for r = 1:R
    
    sum_A = 0;
    for k = 1:n^2
        sum_A = sum_A + Phi{2}(k,r)*A_block{k};
    end
    
    sum_B = 0;
    for ell = 1:n^2
        sum_B = sum_B + Phi{3}(ell,r)*B_block{ell};
    end
    
    pr_AB = rec_CP_matrix_mul(sum_A,sum_B,Phi,m/n,n);
    
    for i = 1:n^2
        C_block{i} = C_block{i} + Phi{1}(i,r) * pr_AB; 
    end    
end

%%%%

C = cell2mat(C_block);