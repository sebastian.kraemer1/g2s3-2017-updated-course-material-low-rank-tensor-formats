function value = operator_bracket(T,I)
n = size(T); % mode sizes
d = length(n); % dimension

% pos = ?
pos = I(d) - 1;
for mu = d-1:-1:1
    pos = pos * n(mu) + (I(mu) - 1);
end
pos = pos + 1;
    
value = T(pos); % value will be returned as output (see first line)
end