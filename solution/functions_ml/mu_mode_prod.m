function T_new = mu_mode_prod(T,d,A,mu)

n = size(T);
n = [n,ones(1,d-length(n))];

T_mu = reshape(  permute(T,[mu,1:mu-1,mu+1:d])  ,[n(mu),prod(n(1:mu-1))*prod(n(mu+1:d))]);
T_new_mu = A*T_mu;

n(mu) = size(A,1);
T_new = permute(  reshape(T_new_mu,[n(mu),n(1:mu-1),n(mu+1:d)])  ,[1+(1:mu-1),1,mu+1:d]);

end