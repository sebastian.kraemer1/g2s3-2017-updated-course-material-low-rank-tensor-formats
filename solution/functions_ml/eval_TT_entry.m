function H = eval_TT_entry(G,I,n)

H = 1;
d = length(n);

for mu = d:-1:1
    H = G{mu}(:,:,I(mu)) * H;
end