function C = fast_CP_matrix_mul(A,B,Phi,m,n)

temp = reshape(A,[m,n,m,n]);
A_block = reshape(permute(temp,[2,4,1,3]),[n^2,m^2]);

temp = reshape(B,[m,n,m,n]);
B_block = reshape(permute(temp,[2,4,1,3]),[n^2,m^2]);

R = size(Phi{1},2);

%% using only one for loop
Phi2_A = A_block'*Phi{2}; 
Phi3_B = B_block'*Phi{3}; 

temp_A = reshape(Phi2_A,[m,m,R]);
temp_B = reshape(Phi3_B,[m,m,R]);

prAB = zeros(m,m,R);
for r = 1:R
    prAB(:,:,r) = temp_A(:,:,r) * temp_B(:,:,r);
end
prAB = reshape(prAB,[m^2,R]);
temp = reshape(prAB*Phi{1}',[m,m,n,n]);
C = reshape(permute(temp,[1,3,2,4]),[m*n,m*n]);

end