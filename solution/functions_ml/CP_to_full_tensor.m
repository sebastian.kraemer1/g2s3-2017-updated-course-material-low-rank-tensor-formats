% function T = CP_to_full_tensor(Phi)
% % note that you can call mode_1_kron from inside this function
% d = length(Phi);
% 
% T = 'ANSWER 2 MISSING';
% 
% end


function T = CP_to_full_tensor(Phi)

d = length(Phi);
if d == 2
    T = Phi{1}*Phi{2}';
    return
end

n = zeros(1,d);
for mu = 1:d
    n(mu) = size(Phi{mu},1);
end

T = mode_1_kron(Phi{1},Phi{2});
for mu = 3:d-1
    T = mode_1_kron(T,Phi{mu});
end
T = T*Phi{d}';
T = reshape(T,n);

end