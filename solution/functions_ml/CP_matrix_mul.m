function C = CP_matrix_mul(A,B,Phi,m,n)

A_block = mat2cell(A,m*ones(1,n),m*ones(1,n));
B_block = mat2cell(B,m*ones(1,n),m*ones(1,n));
C_block = repmat({zeros(m,m)},[n,n]);

R = size(Phi{1},2);

%%%% solution EX3a)

for r = 1:R
    
    sum_A = 0;
    for k = 1:n^2
        sum_A = sum_A + Phi{2}(k,r)*A_block{k};
    end
    
    sum_B = 0;
    for ell = 1:n^2
        sum_B = sum_B + Phi{3}(ell,r)*B_block{ell};
    end
    
    pr_AB = sum_A * sum_B; % only matrix multiplication
    
    for i = 1:n^2
        C_block{i} = C_block{i} + Phi{1}(i,r) * pr_AB; 
    end    
end

%%%%

C = cell2mat(C_block);


