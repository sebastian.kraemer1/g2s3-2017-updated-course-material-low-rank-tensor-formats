% function z = mode_1_kron(v,w)
% n1 = size(v,1);
% n2 = size(w,1);
% k = size(v,2);
% 
% z = 'ANSWER 1 MISSING';
% end

function z = mode_1_kron(v,w)
n1 = size(v,1);
n2 = size(w,1);
k = size(v,2);
z = zeros(n1,n2,k);
for i = 1:k
    z(:,:,i) = v(:,i)*w(:,i)';
end
z = reshape(z,[n1*n2,k]);
end