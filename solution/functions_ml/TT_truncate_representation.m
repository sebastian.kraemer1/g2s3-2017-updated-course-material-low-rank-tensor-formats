function [G,r] = TT_truncate_representation(G,tol)

d = length(G);
r = ones(1,d+1);
n = zeros(1,d);

for mu = d:-1:2
    [r(mu),r(mu+1),n(mu)] = size(G{mu});
    [Q,R] = qr(right_fold(G{mu})',0);
    G{mu} = right_unfold(Q',r(mu),r(mu+1),n(mu));
    G{mu-1} = boxtimes(G{mu-1},R');
end
[r(1),r(1+1),n(1)] = size(G{1});

for mu = 1:d-1   
    [U,S,V] = svd(left_fold(G{mu}),0);
    r(mu+1) = sum(diag(S)>tol); % count number of entries larger than tol
    G{mu} = left_unfold(U(:,1:r(mu+1)),r(mu),r(mu+1),n(mu));
    G{mu+1} = boxtimes(S(1:r(mu+1),1:r(mu+1)) * V(:,1:r(mu+1))',G{mu+1});
end